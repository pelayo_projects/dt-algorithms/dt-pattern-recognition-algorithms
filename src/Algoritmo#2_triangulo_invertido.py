from time import time
import collections, itertools
from itertools import permutations, combinations
from random import shuffle
from stationsObjects import *
from collections import defaultdict
import pandas as pd
import json


# FUNCIONES PARA DESARROLLO DE ALGORITMO

def doble2simple(muones):
    muones_divididos = []
    for muon in muones:

        d = collections.defaultdict(list)

        for x in muon:
            d[x[0]].append(x)

        l = list(itertools.product(*d.values()))

        for i in range(len(l)):
            l[i] = list(l[i])

        muones_divididos.extend(l)

    return muones_divididos


def count_elapsed_time(f):
    """
    Decorator.
    Execute the function and calculate the elapsed time.
    Print the result to the standard output.
    """

    def wrapper():
        # Start counting.
        start_time = time()
        # Take the original function's return value.
        ret = f()
        # Calculate the elapsed time.
        elapsed_time = time() - start_time
        print("Elapsed time: %0.10f seconds." % elapsed_time)
        return ret

    return wrapper


# Generate true muons and its resulting set of patterns


def buscar(lista, y):
    hits_1 = []

    for sublista in lista:
        if sublista[0] == y:
            hits_1.append(sublista)

    return hits_1


import collections, itertools


def cell2xy(hit):
    if hit[0] >= 5:
        ypos = (hit[0] - 1) * globalDTheight + globalDTheight / 2 + SLgap
    else:

        ypos = (hit[0] - 1) * globalDTheight + globalDTheight / 2

    if hit[0] % 2 == 0:
        xpos = (hit[1]) * globalDTwidth + globalDTwidth
    else:
        xpos = (hit[1]) * globalDTwidth + globalDTwidth / 2

    return ypos, xpos


def xy2cell(hit):
    if hit[0] >= 5:
        ypos = (hit[0] - globalDTheight / 2 - SLgap) / globalDTheight + 1
    else:
        ypos = (hit[0] - globalDTheight / 2) / globalDTheight + 1

    if round(hit[0], 3) in [1.95, 4.55, 25.75, 28.35]:
        xpos = (hit[1] - globalDTwidth) / globalDTwidth
    else:
        xpos = (hit[1] - globalDTwidth / 2) / globalDTwidth

    return [round(ypos), round(xpos)]


def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


# def obtain_true_muons()
def muon_quality(muones):
    # LISTA DE CALIDADES
    # LISTA[0] MAXIMA CALIDAD (8 HITS)
    # LISTA[1] CALIDAD ALTA
    # LISTA[2] CALIDAD MEDIA
    # LISTA[3] CALIDAD BAJA

    lista = [[], [], [], []]

    for muon in muones:
        if len(muon) == 8:
            if muon not in lista[0]:
                lista[0].append(muon)
                print(f"Adding MAX QUALITY MUON  {muon}")
        elif len(muon) == 7:
            if muon not in lista[1]:
                lista[1].append(muon)
                print(f"Adding HIGH QUALITY MUON  {muon}")
        elif len(muon) == 6:
            if muon not in lista[2]:
                lista[2].append(muon)
                print(f"Adding MEDIUM QUALITY MUON  {muon}")
        elif len(muon) == 5:
            if muon not in lista[3]:
                lista[3].append(muon)
                print(f"Adding LOW QUALITY MUON  {muon}")

    return lista


def count_quality_muons(muons_q, SL):
    if SL:
        for i in range(4):
            n_muons = len(muons_q[i])
            if i == 0:
                quality = "MAX"
            elif i == 1:
                quality = "HIGH"
            elif i == 2:
                quality = "MEDIUM"
            elif i == 3:
                quality = "LOW"
    else:
        for i in range(2):
            n_muons = len(muons_q[i])
            if i == 0:
                quality = "HIGH"
            elif i == 1:
                quality = "LOW"

    print(f"There are {n_muons} muons with QUALITY: {quality} \n")


def combinacion_posible(muon):
    desplazamiento = [0, ]
    origen = muon[0][1]
    origen_l = muon[0][0]

    Posible = True
    if len(muon) > 4:
        origen2 = muon[4][1]

    for k in range(1, len(muon)):
        if not abs(muon[k][0] - muon[k - 1][0]) == 1:
            return True

    for i in range(1, len(muon)):

        if i < 4:
            diferencia = muon[i][1] - origen
        else:
            diferencia = muon[i][1] - origen2
        if origen_l == 2 or origen_l == 6:
            if i % 2 == 0:

                if diferencia == 1:
                    desplazamiento.append(2)
                elif diferencia == 0:
                    desplazamiento.append(0)
                else:
                    desplazamiento.append(-2)
            else:
                if diferencia == 0:
                    desplazamiento.append(-1)
                elif diferencia == 1:
                    desplazamiento.append(1)



        else:

            if i % 2 == 0:

                if diferencia > 0:
                    desplazamiento.append(2)
                elif diferencia < 0:
                    desplazamiento.append(-2)
                else:
                    desplazamiento.append(0)
            else:
                if diferencia > 0:
                    desplazamiento.append(3)
                elif diferencia == 0:
                    desplazamiento.append(1)
                elif diferencia < -1:
                    desplazamiento.append(-3)
                else:
                    desplazamiento.append(-1)

    for i in range(len(desplazamiento) - 1):
        if abs(desplazamiento[i] - desplazamiento[i + 1]) > 1:
            Posible = False
    return Posible


# FUNCION QUE TRATA DE RECONOCER UN PATRON DESDE CADA HIT EN LA L1
# TRATANDO DE BUSCAR EN LAS 9 CELDAS SUPERIORES, DISTRIBUIDAS EN FORMA DE TRIANGULO
# ALGORITMO PRINCIPAL DE BUSQUEDA
def buscar_patron(hits_SL, n_celdas):
    posibles_muones = []
    buffer_triangulos = []
    quality = [[], []]
    layer_4 = [-1, 0, 1, 2]
    layer_3 = [-1, 0, 1]
    layer_2 = [0, 1]
    layer_1 = [0]

    for i in range(n_celdas):
        buffer_triangulos.append([])

    for hit in hits_SL:
        if hit[0] == 4 or hit[0] == 8:
            for position in layer_4:
                if not (hit[1] + position) > (n_celdas - 1) or (hit[1] + position) < 0:
                    if hit not in buffer_triangulos[hit[1] + position]:
                        buffer_triangulos[hit[1] + position].append(hit)
        elif hit[0] == 3 or hit[0] == 7:
            for position in layer_3:
                a = hit[1] + position
                if not (hit[1] + position) > (n_celdas - 1) or (hit[1] + position) < 0:
                    if hit not in buffer_triangulos[hit[1] + position]:
                        buffer_triangulos[hit[1] + position].append(hit)
        elif hit[0] == 2 or hit[0] == 6:
            for position in layer_2:
                if not (hit[1] + position) > (n_celdas - 1) or (hit[1] + position) < 0:
                    if hit not in buffer_triangulos[hit[1] + position]:
                        buffer_triangulos[hit[1] + position].append(hit)
        elif hit[0] == 1 or hit[0] == 5:
            for position in layer_1:
                if not (hit[1] + position) > (n_celdas - 1) or (hit[1] + position) < 0:
                    if hit not in buffer_triangulos[hit[1] + position]:
                        buffer_triangulos[hit[1] + position].append(hit)

    for triangulo in buffer_triangulos:
        if len(triangulo) > 2:
            triangulo.sort(key=lambda x: (x[0], x[1]))
            posibles_muones.append(triangulo)

    posibles_muones = doble2simple(posibles_muones)

    for muon in posibles_muones:

        es_bueno = combinacion_posible(muon)

        if len(muon) == 4 and es_bueno:

            quality[0].append(muon)

        elif len(muon) == 3 and es_bueno:

            quality[1].append(muon)

    return (quality)


def extract_quality(list, quality_level):
    if quality_level == "MAX":
        return list[0]
    elif quality_level == "HIGH":
        return list[1]
    elif quality_level == "MEDIUM":
        return list[2]
    elif quality_level == "LOW":
        return list[3]
    elif quality_level == "AGING":
        list_mix = []
        list_mix.extend(list[0])
        list_mix.extend(list[1])

        return list_mix


def hay_hits(muon, hits):
    if not hits:
        return 0
    sl2_np = np.array(hits)[:, 0, 1]
    idxs = []
    Ubicaciones = defaultdict(list)
    for index in range(len(sl2_np)):
        Ubicaciones[sl2_np[index]].append(index)

    y1, x1 = cell2xy(muon[0])
    y2, x2 = cell2xy(muon[-1])

    # Necesiro modificar los valores de x2 para aumentar el area de busqueda

    x2_1 = x2 - globalDTwidth / 2
    x2_2 = x2 + globalDTwidth / 2

    y = 24.45

    if not round(x2_1 - x1) == 0:
        m1 = (y2 - y1) / (x2_1 - x1)
        x2_1_top = ((y - y1) / m1) + x1
    else:
        x2_1_top = x1 + globalDTwidth

    if not round(x2_2 - x1) == 0:
        m2 = (y2 - y1) / (x2_2 - x1)
        x2_2_top = ((y - y1) / m2) + x1
    else:
        x2_2_top = x1 + globalDTwidth

    y = 24.45

    cell_x21 = xy2cell([y, x2_1_top])
    cell_x22 = xy2cell([y, x2_2_top])
    if cell_x21[1] > 46:
        cell_x21[1] = 46
    elif cell_x21[1] < 0:
        cell_x21[1] = 0
    if cell_x22[1] > 46:
        cell_x22[1] = 46
    elif cell_x22[1] < 0:
        cell_x22[1] = 0
    for i in range(cell_x21[1], cell_x22[1] + 1):
        if i in list(sl2_np):
            idxs.extend(Ubicaciones[i])

    return idxs


def correlate_superlayers(SL1, SL2):
    muones_finales = []

    for muon in SL1:

        idxs = hay_hits(muon, SL2)

        if not idxs:
            muones_finales.append(muon)
        else:

            for idx in idxs:
                muon_correlated = []
                muon_correlated.extend(muon)
                muon_correlated.extend(SL2[idx])
                muones_finales.append(muon_correlated)
                print(muon_correlated)
                print("-------------")

    print("*****")
    return (muones_finales)


def compare_with_reality(trueMuons, algorithmMuons):
    total = len(trueMuons)
    matched = 0
    already_matched = []

    for muon in algorithmMuons:
        for real_muon in trueMuons:
            if muon == real_muon and not muon in already_matched:
                matched += 1
                already_matched.append(muon)

    wrong = total - matched
    if not total == 0:
        percentage = matched / total * 100
    else:
        percentage = 0

    print(f"""RESULTS: \n
            *************************
            NUMBER OF TRUE MUONS : {total} \n
            NUMBER OF PREDICTED MUONS : {len(algorithmMuons)} \n
            NUMBER OF MUONS NOT PREDICTED: {wrong} \n
            PERCENTAGE OF CORRECTNESS: {percentage} \n
            NOISE HITS : {nNoise}
            *************************
""")
    return total, wrong, percentage


def truehits_SL(truehits, semicells):
    true_SL1 = []
    true_SL2 = []
    lat_SL1 = []
    lat_SL2 = []

    for hit in truehits:
        if len(hit) == 4:
            if combinacion_posible(hit):
                idx = truehits.index(hit)
                true_SL1.append(hit)
                lat_SL1.append(semicells[idx])
        else:
            for i in range(0, len(hit)):
                a = hit[i][0]
                if a > 4:
                    if hit[:i] not in true_SL1 and combinacion_posible(hit[:i]):
                        true_SL1.append(hit[:i])
                        idx = truehits.index(hit)
                        lat_SL1.append(semicells[idx][:i])
                    if hit[i:] not in true_SL2 and combinacion_posible(hit[i:]):
                        true_SL2.append(hit[i:])
                        idx = truehits.index(hit)
                        lat_SL2.append(semicells[idx][i:])
                    break

    return true_SL1, true_SL2, lat_SL1, lat_SL2


def pos_relativa(muon):
    # tomo como origen la primera celda del triangulo (0) y calculo la posicion relativa de las demas en unidades de sem
    # icelda
    desplazamiento = [0, ]
    origen = muon[0][1]

    for i in range(1, len(muon)):

        diferencia = muon[i][1] - origen

        if i % 2 == 0:

            if diferencia > 0:
                desplazamiento.append(2)
            elif diferencia < 0:
                desplazamiento.append(-2)
            else:
                desplazamiento.append(0)
        else:
            if diferencia > 0:
                desplazamiento.append(3)
            elif diferencia == 0:
                desplazamiento.append(1)
            elif diferencia < -1:
                desplazamiento.append(-3)
            else:
                desplazamiento.append(-1)

    return desplazamiento


def lateralidades(muon):
    # supongo 0 izquierda y 1 derecha

    combinaciones_posibles = []
    combinaciones_reales = []
    for a in range(0, 2):
        for b in range(0, 2):
            for c in range(0, 2):
                for d in range(0, 2):
                    full = [a, b, c, d]
                    combinaciones_posibles.append(full)

    combinaciones_pos = []

    pos_rel = np.asarray(pos_relativa(muon))

    for combinacion in combinaciones_posibles:

        comb = np.asarray(combinacion)

        prueba = pos_rel + comb
        desfase = []
        desfase = np.asarray(desfase)
        diff = []
        diff = np.asarray(diff)
        for i in range(0, len(prueba) - 1):
            desfase = np.append(desfase, prueba[i + 1] - prueba[i])

        for i in range(0, len(desfase) - 1):
            diff = np.append(diff, (abs(desfase[i + 1] - desfase[i])))

        diff = np.append(diff, abs(desfase[2] - desfase[0]))

        resultado = (diff[0] > 1 or diff[1] > 1 or diff[2] > 1)

        if not resultado:
            combinaciones_pos.append(combinacion)

    for combinacion in combinaciones_pos:
        for i in range(0, len(combinacion)):

            if combinacion[i] == 0:
                combinacion[i] = -1
            elif combinacion[i] == 1:
                combinacion[i] = 1

    return combinaciones_pos


def edit_aging_truehits(truemuons, aging):
    for hit in aging:
        for truemuon in truemuons:
            for cell in truemuon:
                if cell == hit:
                    truemuon.remove(cell)
    for muon in truemuons:
        if len(muon) < 3:
            truemuons.remove(muon)


def compare_aging(patronesSL1, patronesSL2, trueSL1, trueSL2, hitsAging):
    total_SL1 = len(trueSL1)
    total_SL2 = len(trueSL2)
    edit_aging_truehits(trueSL1, hitsAging)
    edit_aging_truehits(trueSL2, hitsAging)
    matched_sl1 = 0
    already_matched_sl1 = []
    matched_sl2 = 0
    already_matched_sl2 = []

    for muon in patronesSL1:
        for real_muon in trueSL1:
            if muon == real_muon and not muon in already_matched_sl1:
                matched_sl1 += 1
                already_matched_sl1.append(muon)
    for muon in patronesSL2:
        for real_muon in trueSL2:
            if muon == real_muon and not muon in already_matched_sl2:
                matched_sl2 += 1
                already_matched_sl2.append(muon)

    wrong_sl1 = total_SL1 - matched_sl1
    wrong_sl2 = total_SL2 - matched_sl2
    if not total_SL1 == 0:
        percentage_sl1 = matched_sl1 / total_SL1 * 100
    else:
        percentage_sl1 = 0
    if not total_SL2 == 0:
        percentage_sl2 = matched_sl2 / total_SL2 * 100
    else:
        percentage_sl2 = 0

    return total_SL1, wrong_sl1, percentage_sl1, total_SL2, wrong_sl2, percentage_sl2


def lat_to_truehits(truehits, semicells):
    orden_data = []
    true_lats = []
    for i in range(0, len(truehits)):
        for hit in truehits[i]:
            orden_data.append([hit, semicells[i][truehits[i].index(hit)]])
    real_true = doble2simple(truehits)

    for muon in real_true:
        lat_list = []
        for hit in muon:
            for data in orden_data:
                if hit == data[0]:
                    lat_list.append(data[1])
        true_lats.append(lat_list)

    return real_true, true_lats


def asign_lateralities(patrones):
    patron_lat = []
    numero = 0
    for patron in patrones:
        lateralidad = lateralidades(patron)
        numero = numero + len(lateralidad)
        patron_lat.append([patron, lateralidad])
    return patron_lat, numero


def compare_lateralities(patron_lat, truemuons, truelateralities, numero):
    total_lat = numero
    total_lat_true = len(truelateralities)
    matched = 0
    already_matched = []

    for patron in patron_lat:
        for muon in truemuons:
            if patron[0] == muon:
                idx = truemuons.index(muon)
                for lat in patron[1]:
                    truelat = truelateralities[idx]
                    if lat == truelat:
                        matched += 1

    wrong_lat = total_lat_true - matched
    if not total_lat_true == 0:
        percentage = matched / total_lat_true * 100
    else:
        percentage = 0

    return total_lat, total_lat_true, wrong_lat, percentage


# LISTAS TEST

lateralities_test = []
test_values = []
agin_test_values = []
values_SL = []

# MODOS DE FUNCIONAMIENTO (SOLO 1 = TRUE)
late_mode = False
age_mode = False
correlate_mode = False  # APLICA CORRELACION
SL_mode = True  # CALCULA SOLO SL POR SEPARADO

for k in range(1, 6):  # MUONES REALES
    for w in range(0, 1):  # AGING

        # Global parameters
        np.random.seed(4313937)
        # np.random.seed(1)
        nTrueMuons = k
        nNoise = 0
        reMatchHits = True
        doLaterality = True
        if age_mode:
            if w == 0:
                agingPercentage = 0.94
            elif w == 1:
                agingPercentage = 0.85
            elif w == 2:
                agingPercentage = 0.62
        else:
            agingPercentage = 1
        minHits = 2

        # Loads the patterns
        fig, ax = plt.subplots(1)
        MB1.plot()
        plt.axis([0, 200, -5, 30])

        allHits = []
        trueHits = []
        trueHits2 = []
        trueMuons = []
        alltimes = []
        hitsSL1 = []
        hitsSL2 = []
        ageHits = []
        trueLats = []
        for n in range(nTrueMuons):
            mm = Muon(np.random.rand() * 200, 0., 1. / (np.random.rand()))
            MB1.checkIn(mm)
            trueHits += mm.getRecoPattern()
            trueHits2.append(mm.getRecoPattern())
            trueLats.append(mm.semicells)
            alltimes += mm.getDriftTime()

            mm.color = "r--"
            mm.plot()
            trueMuons.append(mm)
            print(mm.y0, mm.x0, mm.m)

        trueH_SL1, trueH_SL2, lat_SL1, lat_SL2 = truehits_SL(trueHits2, trueLats)
        trueH_SL1, lat_SL1 = lat_to_truehits(trueH_SL1, lat_SL1)
        trueH_SL2, lat_SL2 = lat_to_truehits(trueH_SL2, lat_SL2)

        # And now generate a couple of points with random noise
        noiseHits = []
        for n in range(nNoise):
            noiseHits.append([int(np.ceil(np.random.rand() * 8)), int(np.floor(np.random.rand() * 47))])

        for t in trueHits:
            if np.random.rand() > agingPercentage:
                ageHits.append(t)
                continue
            plt.plot(MB1.layers[t[0] - 1].DTlist[t[1]].center()[0], MB1.layers[t[0] - 1].DTlist[t[1]].center()[1], "gx",
                     markersize=12, linewidth=4)
            allHits.append(t)

        for n in noiseHits:
            if n in trueHits: continue
            plt.plot(MB1.layers[n[0] - 1].DTlist[n[1]].center()[0], MB1.layers[n[0] - 1].DTlist[n[1]].center()[1], "rx",
                     markersize=12, linewidth=4)
            allHits.append(n)

        # Now do the magic

        # First sort all hits according to the layer
        allHits.sort(key=lambda x: x[0])
        alltimes.sort(key=lambda k: [k[1], k[0]])
        shuffle(allHits)
        for hit in allHits:
            if hit[0] < 5:
                hitsSL1.append(hit)
            else:
                hitsSL2.append(hit)
        # First try to separate them:
        print("START: ", allHits)

        posicion = 0
        while posicion < len(trueHits2):
            if not combinacion_posible(trueHits2[posicion]):
                trueHits2.pop(posicion)
            else:
                posicion = posicion + 1

        start_time = time()
        if age_mode:
            posibles_patrones_sl2 = extract_quality(buscar_patron(hitsSL2, 47), "AGING")
            posibles_patrones_sl1 = extract_quality(buscar_patron(hitsSL1, 47), "AGING")
        else:

            posibles_patrones_sl2 = extract_quality(buscar_patron(hitsSL2, 47), "MAX")
            t_SL2 = time() - start_time
            start_time_SL1 = time()
            posibles_patrones_sl1 = extract_quality(buscar_patron(hitsSL1, 47), "MAX")
            t_SL1 = time() - start_time_SL1
            data_SL1 = compare_with_reality(trueH_SL1, posibles_patrones_sl1)
            data_SL2 = compare_with_reality(trueH_SL2, posibles_patrones_sl2)

        if late_mode:
            lateralidades_SL1 = asign_lateralities(posibles_patrones_sl1)
            lateralidades_SL2 = asign_lateralities(posibles_patrones_sl2)
            lat_test_sl1 = compare_lateralities(lateralidades_SL1[0], trueH_SL1, lat_SL1, lateralidades_SL1[1])
            lat_test_sl2 = compare_lateralities(lateralidades_SL2[0], trueH_SL2, lat_SL2, lateralidades_SL2[1])
            lateralities_test.append([k, lat_test_sl1[0], lat_test_sl1[1], lat_test_sl1[2], lat_test_sl1[3],
                                      lat_test_sl2[0], lat_test_sl2[1], lat_test_sl2[2], lat_test_sl2[3]])
        if age_mode:
            aging_test = compare_aging(posibles_patrones_sl1, posibles_patrones_sl2, trueH_SL1, trueH_SL2, ageHits)
            agin_test_values.append(
                [k, agingPercentage, aging_test[0], aging_test[1], aging_test[2], aging_test[3], aging_test[4],
                 aging_test[5]])

        if correlate_mode:
            muones_finales = correlate_superlayers(posibles_patrones_sl1, posibles_patrones_sl2)
            elapsed_time = time() - start_time
            data_test = compare_with_reality(trueHits2, muones_finales)

            test_values.append([k, elapsed_time, len(muones_finales), data_test[0], data_test[1], data_test[2], w])
        if SL_mode:
            values_SL.append([k, t_SL1, len(posibles_patrones_sl1), data_SL1[0], data_SL1[1], data_SL1[2], t_SL2,
                              len(posibles_patrones_sl2), data_SL2[0], data_SL2[1], data_SL2[2], w])

# EXPORTAR DATOS A CSV

if SL_mode:
    d = {'No.muones reales': [], 'Tiempo ejecucion SL1': [], 'Patrones totales SL1': [], 'No.muones reales DH SL1': [],
         'Muones no detectados SL1': [], '% acierto SL1': [], 'Tiempo ejecucion SL2': [], 'Patrones totales SL2': [],
         'No.muones reales DH SL2': [],
         'Muones no detectados SL2': [], '% acierto SL2': [], 'Noise': []}
    for x in values_SL:
        d['No.muones reales'].append(x[0])
        d['Tiempo ejecucion SL1'].append(x[1])
        d['Patrones totales SL1'].append(x[2])
        d['No.muones reales DH SL1'].append(x[3])
        d['Muones no detectados SL1'].append(x[4])
        d['% acierto SL1'].append(x[5])
        d['Tiempo ejecucion SL2'].append(x[6])
        d['Patrones totales SL2'].append(x[7])
        d['No.muones reales DH SL2'].append(x[8])
        d['Muones no detectados SL2'].append(x[9])
        d['% acierto SL2'].append(x[10])
        d['Noise'].append(x[11])

    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_triangle_SL.csv',
              index=None)

if correlate_mode:
    d = {'No.muones reales': [], 'Tiempo ejecucion': [], 'Patrones totales': [], 'No.muones reales DH': [],
         'Muones no detectados': [], '% acierto': [], 'Noise': []}
    for x in test_values:
        d['No.muones reales'].append(x[0])
        d['Tiempo ejecucion'].append(x[1])
        d['Patrones totales'].append(x[2])
        d['No.muones reales DH'].append(x[3])
        d['Muones no detectados'].append(x[4])
        d['% acierto'].append(x[5])
        d['Noise'].append(x[6])

    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_triangle_correlate.csv',
              index=None)

if age_mode:
    d = {'No.muones reales': [], 'Aging': [], 'Patrones totales SL1': [], 'Muones no detectados SL1': [],
         'Efectividad sl1': [], 'Patrones totales SL2': [], 'Muones no detectados SL2': [],
         'Efectividad sl2': []}
    for x in agin_test_values:
        d['No.muones reales'].append(x[0])
        d['Aging'].append(x[1])
        d['Patrones totales SL1'].append(x[2])
        d['Muones no detectados SL1'].append(x[3])
        d['Efectividad sl1'].append(x[4])
        d['Patrones totales SL2'].append(x[5])
        d['Muones no detectados SL2'].append(x[6])
        d['Efectividad sl2'].append(x[7])

    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_triangle_aging.csv',
              index=None)

if late_mode:
    d = {'No.Muones reales': [], 'No.lateralidades totales SL1': [], 'No.lateralidades SL1': [],
         'Lat. no detectadas SL1': [], 'Efectividad LAT SL1': [],
         'No.lateralidades totales SL2': [], 'No.lateralidades SL2': [], 'Lat. no detectadas SL2': [],
         'Efectividad LAT SL2': []}
    for x in lateralities_test:
        d['No.Muones reales'].append(x[0])
        d['No.lateralidades totales SL1'].append(x[1])
        d['No.lateralidades SL1'].append(x[2])
        d['Lat. no detectadas SL1'].append(x[3])
        d['Efectividad LAT SL1'].append(x[4])
        d['No.lateralidades totales SL2'].append(x[5])
        d['No.lateralidades SL2'].append(x[6])
        d['Lat. no detectadas SL2'].append(x[7])
        d['Efectividad LAT SL2'].append(x[8])

    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_triangle_lateral.csv',
              index=None)

plt.show()
