from itertools import permutations, combinations
from time import time

from stationsObjects import *
from time import time
import collections, itertools
from itertools import permutations, combinations
from random import shuffle
from stationsObjects import *
from collections import defaultdict
import pandas as pd
import json

# --------------DECLARACION DE FUNCIONES PARA ALGORITMO------------
class trazaMuon(object):
    def __init__(self, celdass, lateralidadess, posicioness):

        self.celdas = np.array(celdass)
        self.lateralidades = np.array(lateralidadess)
        self.posiciones = np.array(posicioness)
        self.valido = True
        self.posibles_posiciones = []
        self.m = 0
        self.x_pos = []
        self.y_pos = []

    def descartar_muones(self):
        valido = [False, False, False, False]
        for posicion in self.posiciones:
            if posicion[2] > 2.1:
                x = np.where(self.posiciones == posicion[2])
                x = x[0]
                self.posiciones = np.delete(self.posiciones, x, axis=0)
        for posicion in self.posiciones:
            p = posicion[0]
            if p == 1:
                valido[0] = True
            elif p == 2:
                valido[1] = True
            elif p == 3:
                valido[2] = True
            elif p == 4:
                valido[3] = True
            if p == 5:
                valido[0] = True
            elif p == 6:
                valido[1] = True
            elif p == 7:
                valido[2] = True
            elif p == 8:
                valido[3] = True
        if not valido == [True, True, True, True]:
            self.valido = False

    def generar_posiciones_relativas(self):
        if self.valido:
            position_list = self.posiciones.tolist()
            position_list.sort(key=lambda x: x[0])
            combine = list(combinations(position_list, 4))
            posibles_comb = []
            for combinacion in combine:

                if ((combinacion[0][0] == 1 and combinacion[1][0] == 2 and combinacion[2][0] == 3 and
                     combinacion[3][0] == 4 and combinacion not in posibles_comb) or
                        (combinacion[0][0] == 5 and combinacion[1][0] == 6 and combinacion[2][0] == 7 and
                         combinacion[3][0] == 8 and combinacion not in posibles_comb)):
                    posibles_comb.append(combinacion)

            posibles_comb = np.array(posibles_comb)

            for combinacion in posibles_comb:

                for lateralidad in self.lateralidades:
                    vect_lateral = []

                    distancia_relativa = []
                    distancia_abs = []

                    for lado in lateralidad:
                        if lado == 'I':
                            vect_lateral.append(-1)
                        else:
                            vect_lateral.append(1)
                    for comb in combinacion:
                        distancia_relativa.append(comb[2])

                    distancia_relativa = np.array(distancia_relativa)
                    vect_lateral = np.array(vect_lateral)

                    distancia_relativa *= vect_lateral
                    for i in range(0, 4):

                        if self.celdas[i][0] > 4:
                            ypos = (self.celdas[i][0] - 1) * globalDTheight + globalDTheight / 2 + SLgap
                        else:

                            ypos = (self.celdas[i][0] - 1) * globalDTheight + globalDTheight / 2

                        if self.celdas[i][0] % 2 == 0:
                            xpos_center = (self.celdas[i][1]) * globalDTwidth + globalDTwidth
                        else:
                            xpos_center = (self.celdas[i][1]) * globalDTwidth + globalDTwidth / 2

                        distancia_abs.append([ypos, xpos_center])

                    distancia_abs = np.array(distancia_abs)

                    distancia_abs[:, 1] += distancia_relativa

                    self.posibles_posiciones.append(distancia_abs.tolist())

            for grupo in self.posibles_posiciones:
                for posicion in grupo:
                    self.y_pos.append(posicion[0])
                    self.x_pos.append(posicion[1])

    def sacar_pendientes(self):
        if self.valido == True:
            pendientes = []

            error = []

            chi2 = []

            for grupo in self.posibles_posiciones:
                x = []
                y = []
                for k in grupo:
                    x.append(k[1])
                    y.append(k[0])
                x = np.array(x)
                y = np.array(y)

                p = np.polyfit(x, y, 1, full=True)

                error.append(p[1])

                pendientes.append(p[0][0])

                XPos = (x[0] + x[3]) / 2;

                chi = 0
                mu = p[0][0]
                b = XPos

                basewireypos = -1.5 * globalDTheight

                for i in range(0, 4):
                    zi = basewireypos + globalDTheight * i
                    xi = x[i]

                    factor = xi - mu * zi - b

                    chi += (factor * factor)

                chi2.append(chi)

            error = np.array(error)
            pendientes = np.array(pendientes)


            min_error_idx = np.where(error == np.amin(error))

            pos = self.posibles_posiciones[np.asscalar(min_error_idx[0])]

            self.m = pendientes[np.asscalar(min_error_idx[0])]
            print("Pendiente posible-Posicion puntos")
            print(self.m, pos)


# BUSCA LOS HITS DE UNA CAPA DETERMINADA
def buscar(lista, y):
    hits_1 = []

    for sublista in lista:
        if sublista[0] == y:
            hits_1.append(sublista)

    return hits_1


print("--------------------------------------------------------")


def cell2xy(hit):
    if hit[0] >= 5:
        ypos = (hit[0] - 1) * globalDTheight + globalDTheight / 2 + SLgap
    else:

        ypos = (hit[0] - 1) * globalDTheight + globalDTheight / 2

    if hit[0] % 2 == 0:
        xpos = (hit[1]) * globalDTwidth + globalDTwidth
    else:
        xpos = (hit[1]) * globalDTwidth + globalDTwidth / 2

    return ypos, xpos


def xy2cell(hit):
    if hit[0] >= 5:
        ypos = (hit[0] - globalDTheight / 2 - SLgap) / globalDTheight + 1
    else:
        ypos = (hit[0] - globalDTheight / 2) / globalDTheight + 1

    if round(hit[0], 3) in [1.95, 4.55, 25.75, 28.35]:
        xpos = (hit[1] - globalDTwidth) / globalDTwidth
    else:
        xpos = (hit[1] - globalDTwidth / 2) / globalDTwidth

    return [round(ypos), round(xpos)]


def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx

# FUNCION QUE TRATA DE RECONOCER UN PATRON DESDE CADA HIT EN LA L1
# TRATANDO DE BUSCAR EN LAS 9 CELDAS SUPERIORES, DISTRIBUIDAS EN FORMA DE TRIANGULO

def buscar_patron(layer1, layer2, layer3, layer4):
    posibles_muones = []
    combinaciones = []
    muones = []

    for i in range(0, len(layer1)):
        n = layer1[i][1]
        posible_muon = [layer1[i]]

        # LAYER 2
        for celda in layer2:
            # CELDA IZQUIERDA L2
            if celda[1] == n - 1 and celda not in posible_muon:
                posible_muon.append(celda)
                # LAYER 3 DESDE CELDA IZQUIERDA DE L2
                for celda in layer3:
                    # CELDA IZQUIERDA L3
                    if celda[1] == n - 1 and celda not in posible_muon:
                        posible_muon.append(celda)
                        # LAYER 4 DESDE CELDA IZQUIERDA DE L3
                        for celda in layer4:
                            # CELDA IZQUIERDA L4
                            if celda[1] == n - 2 and celda not in posible_muon:
                                posible_muon.append(celda)
                            # CELDA DERECHA L4
                            elif celda[1] == n - 1 and celda not in posible_muon:
                                posible_muon.append(celda)
                    # CELDA DERECHA L3
                    elif celda[1] == n and celda not in posible_muon:
                        posible_muon.append(celda)
                        # LAYER 4 DESDE CELDA DERECHA DE L3
                        for celda in layer4:
                            # CELDA IZQUIERDA L4
                            if celda[1] == n - 1 and celda not in posible_muon:
                                posible_muon.append(celda)
                            # CELDA DERECHA L4
                            elif celda[1] == n and celda not in posible_muon:
                                posible_muon.append(celda)


            # CELDA DERECHA L2
            elif celda[1] == n and celda not in posible_muon:
                posible_muon.append(celda)
                # LAYER 3 DESDE CELDA DERECHA DE L2
                for celda in layer3:
                    # CELDA IZQUIERDA L3
                    if celda[1] == n and celda not in posible_muon:
                        posible_muon.append(celda)
                        # LAYER 4 DESDE CELDA IZQUIERDA L3
                        for celda in layer4:
                            # CELDA IZQUIERDA L4
                            if celda[1] == n - 1 and celda not in posible_muon:
                                posible_muon.append(celda)
                            # CELDA DERECHA L4
                            elif celda[1] == n and celda not in posible_muon:
                                posible_muon.append(celda)
                    # CELDA DERECHA L3
                    elif celda[1] == n + 1 and celda not in posible_muon:
                        posible_muon.append(celda)
                        # LAYER 4 DESDE CELDA DERECHA L3
                        for celda in layer4:
                            # CELDA IZQUIERDA L4
                            if celda[1] == n and celda not in posible_muon:
                                posible_muon.append(celda)
                            # CELDA DERECHA L4
                            elif celda[1] == n + 1 and celda not in posible_muon:
                                posible_muon.append(celda)


        posibles_muones.append(posible_muon)
    print(posibles_muones)

    # **************************************************************
    # Compruebo que cada supuesto muon tiene 4 hits v2
    # **************************************************************
    for i in range(0, len(posibles_muones)):

        # si hay mas de 4 hits, puede haber varios dobles, por lo que calculo todas las combinaciones posibles
        if len(posibles_muones[i]) > 4:

            combinaciones.append(list(permutations(posibles_muones[i], 4)))

        elif len(posibles_muones[i]) == 4:

            muones.append(posibles_muones[i])

    # solo me quedo con las combinaciones que tengan la forma [1,x][2,x][3,x][4,x]
    for combinacion in combinaciones:

        for muon in combinacion:

            if (muon[0][0] == 1 and muon[1][0] == 2 and muon[2][0] == 3 and muon[3][0] == 4 and muon not in muones or
                    (muon[0][0] == 5 and muon[1][0] == 6 and muon[2][0] == 7 and muon[3][
                        0] == 8 and muon not in muones)):

                # con esto envito  un muon [1,18][2,18][3,18][4,19], que no tendria sentido.
                if (muon[3][1] - muon[2][1] == 0 or muon[3][1] - muon[2][1] == -1) and (
                        muon[2][1] - muon[1][1] == 0 or muon[2][1] - muon[1][1] == 1):
                    muones.append(muon)

    # Ordeno por nº celda
    muones.sort(key=lambda k: [k[1], k[0]])

    return muones


# BUSCA LA LATERALIDAD DEL MUON EN CUESTION, CON UNA UNIDAD MINIMA DE UNA SEMICELDA
def pos_relativa(muon):
    # tomo como origen la primera celda del triangulo (0) y calculo la posicion relativa de las demas en unidades de sem
    # icelda
    desplazamiento = [0, ]
    origen = muon[0][1]

    for i in range(1, 4):

        diferencia = muon[i][1] - origen

        if i % 2 == 0:

            if diferencia > 0:
                desplazamiento.append(2)
            elif diferencia < 0:
                desplazamiento.append(-2)
            else:
                desplazamiento.append(0)
        else:
            if diferencia > 0:
                desplazamiento.append(3)
            elif diferencia == 0:
                desplazamiento.append(1)
            elif diferencia < -1:
                desplazamiento.append(-3)
            else:
                desplazamiento.append(-1)

    return desplazamiento


def lateralidades(muones):
    # supongo 0 izquierda y 1 derecha

    combinaciones_posibles = []
    combinaciones_reales = []
    for a in range(0, 2):
        for b in range(0, 2):
            for c in range(0, 2):
                for d in range(0, 2):
                    full = [a, b, c, d]
                    combinaciones_posibles.append(full)

    for muon in muones:

        combinaciones_pos = []

        pos_rel = np.asarray(pos_relativa(muon))

        for combinacion in combinaciones_posibles:

            comb = np.asarray(combinacion)

            prueba = pos_rel + comb
            desfase = []
            desfase = np.asarray(desfase)
            diff = []
            diff = np.asarray(diff)
            for i in range(0, len(prueba) - 1):
                desfase = np.append(desfase, prueba[i + 1] - prueba[i])

            for i in range(0, len(desfase) - 1):
                diff = np.append(diff, (abs(desfase[i + 1] - desfase[i])))

            diff = np.append(diff, abs(desfase[2] - desfase[0]))

            resultado = (diff[0] > 1 or diff[1] > 1 or diff[2] > 1)

            if not resultado:
                combinaciones_pos.append(combinacion)

        combinaciones_reales.append(combinaciones_pos)

    for combinacion in combinaciones_reales:
        for lateralidad in combinacion:

            for i in range(0, len(lateralidad)):
                if lateralidad[i] == 0:

                    lateralidad[i] = 'I'

                elif lateralidad[i] == 1:

                    lateralidad[i] = 'D'

    return combinaciones_reales


def obtener_posicion_hit(tiempos, muon):
    vdrift = 0.0542  # mm/ns
    posiciones_absolutas = []
    for tiempo in tiempos:

        for par in muon:

            if tiempo[0] == par[0] and tiempo[1] == par[1]:
                distancia_drift = tiempo[2] * vdrift / 10
                posiciones_absolutas.append([par[0], par[1], distancia_drift])

    return posiciones_absolutas
def doble2simple(muones):
    muones_divididos = []
    for muon in muones:

        d = collections.defaultdict(list)

        for x in muon:
            d[x[0]].append(x)

        l = list(itertools.product(*d.values()))

        for i in range(len(l)):
            l[i] = list(l[i])

        muones_divididos.extend(l)

    return muones_divididos

def combinacion_posible(muon):
    desplazamiento = [0, ]
    origen = muon[0][1]

    Posible = True
    if len(muon) > 4:
        origen2 = muon[4][1]
    for i in range(1, len(muon)):
        if i < 4:
            diferencia = muon[i][1] - origen
        else:
            diferencia = muon[i][1] - origen2

        if i % 2 == 0:

            if diferencia > 0:
                desplazamiento.append(2)
            elif diferencia < 0:
                desplazamiento.append(-2)
            else:
                desplazamiento.append(0)
        else:
            if diferencia > 0:
                desplazamiento.append(3)
            elif diferencia == 0:
                desplazamiento.append(1)
            elif diferencia < -1:
                desplazamiento.append(-3)
            else:
                desplazamiento.append(-1)

    for i in range(len(desplazamiento) - 1):
        if abs(desplazamiento[i] - desplazamiento[i + 1]) > 1:
            Posible = False
    return Posible

def truehits_SL(truehits):
    true_SL1=[]
    true_SL2=[]

    for hit in truehits:
        for i in range(0,len(hit)):
            a=hit[i][0]
            if a >4:
                if hit[:i] not in true_SL1 and combinacion_posible(hit[:i]):
                    true_SL1.append(hit[:i])
                if hit[i:] not in true_SL2 and combinacion_posible(hit[i:]):
                    true_SL2.append(hit[i:])
                break

    return true_SL1,true_SL2
def compare_with_reality(trueMuons, algorithmMuons):
    total = len(trueMuons)
    matched = 0
    already_matched = []

    for muon in algorithmMuons:
        for real_muon in trueMuons:
            if tuple(muon) == tuple(real_muon) and not muon in already_matched:
                matched += 1
                already_matched.append(muon)

    wrong = total - matched
    if not total == 0:
        percentage = matched / total * 100
    else:
        percentage = 0

    print(f"""RESULTS: \n
            *************************
            NUMBER OF TRUE MUONS : {total} \n
            NUMBER OF PREDICTED MUONS : {len(algorithmMuons)} \n
            NUMBER OF MUONS NOT PREDICTED: {wrong} \n
            PERCENTAGE OF CORRECTNESS: {percentage} \n
            NOISE HITS : {nNoise}
            *************************
""")
    return total, wrong, percentage


# FUNCION PARA APLICAR LA CORRELACION QUE PROYECTA EL AREA SOBRE LA SL SUPERIOR PARA VER SI HAY HITS EN EL INTERVALO
def hay_hits(muon, hits):
    if not hits:
        return 0
    sl2_np = np.array(hits)[:, 0, 1]
    idxs = []
    Ubicaciones = defaultdict(list)
    for index in range(len(sl2_np)):
        Ubicaciones[sl2_np[index]].append(index)

    y1, x1 = cell2xy(muon[0])
    y2, x2 = cell2xy(muon[-1])

    # Necesiro modificar los valores de x2 para aumentar el area de busqueda

    x2_1 = x2 - globalDTwidth / 2
    x2_2 = x2 + globalDTwidth / 2

    y = 24.45

    if not round(x2_1 - x1) == 0:
        m1 = (y2 - y1) / (x2_1 - x1)
        x2_1_top = ((y - y1) / m1) + x1
    else:
        x2_1_top = x1 + globalDTwidth

    if not round(x2_2 - x1) == 0:
        m2 = (y2 - y1) / (x2_2 - x1)
        x2_2_top = ((y - y1) / m2) + x1
    else:
        x2_2_top = x1 + globalDTwidth

    y = 24.45

    cell_x21 = xy2cell([y, x2_1_top])
    cell_x22 = xy2cell([y, x2_2_top])
    if cell_x21[1] > 46:
        cell_x21[1] = 46
    elif cell_x21[1] < 0:
        cell_x21[1] = 0
    if cell_x22[1] > 46:
        cell_x22[1] = 46
    elif cell_x22[1] < 0:
        cell_x22[1] = 0
    for i in range(cell_x21[1], cell_x22[1] + 1):
        if i in list(sl2_np):
            idxs.extend(Ubicaciones[i])

    return idxs

# CORRELACION
def correlate_superlayers(SL1, SL2):
    muones_finales = []

    for muon in SL1:

        idxs = hay_hits(muon, SL2)

        if not idxs:
            muones_finales.append(muon)
        else:

            for idx in idxs:
                muon_correlated = []
                muon_correlated.extend(muon)
                muon_correlated.extend(SL2[idx])
                muones_finales.append(muon_correlated)
                print(muon_correlated)
                print("-------------")

    print("*****")
    return (muones_finales)

#MODOS DE FUNCIONAMIENTO
SL_mode=True
correlate_mode=False

#LISTAS
test_values=[]
values_SL=[]
for k in range(1,6):
    for z in range(0,1):
        # Global parameters
        np.random.seed(4313937)
        # np.random.seed(1)
        nTrueMuons = k
        nNoise = z

        reMatchHits = True
        doLaterality = True
        agingPercentage = 1
        minHits = 2

        # Loads the patterns
        # fig, ax = plt.subplots(1)
        # MB1.plot()
        # plt.axis([0, 200, -5, 30])

        # Now test a set of hits


        # Generate true muons and its resulting set of patterns
        allHits = []
        trueHits = []
        trueMuons = []
        alltimes = []
        trueHits2=[]
        trueHits_SL1=[]
        trueHits_SL2=[]
        for n in range(nTrueMuons):
            mm = Muon(np.random.rand() * 200, 0., 1. / (np.random.rand()))
            MB1.checkIn(mm)
            trueHits += mm.getRecoPattern()
            alltimes += mm.getDriftTime()
            trueHits2.append(mm.getRecoPattern())
            mm.color = "r--"
            mm.plot()
            trueMuons.append(mm)
            print(mm.y0, mm.x0, mm.m)
        trueHits2=doble2simple(trueHits2)
        trueHits_SL1,trueHits_SL2=truehits_SL(trueHits2)
        posicion = 0
        while posicion < len(trueHits2):
            if not combinacion_posible(trueHits2[posicion]):
                trueHits2.pop(posicion)
            else:
                posicion = posicion + 1
        # And now generate a couple of points with random noise
        noiseHits = []
        for n in range(nNoise):
            noiseHits.append([int(np.ceil(np.random.rand() * 8)), int(np.floor(np.random.rand() * 47))])

        for t in trueHits:
            if np.random.rand() > agingPercentage: continue
            plt.plot(MB1.layers[t[0] - 1].DTlist[t[1]].center()[0], MB1.layers[t[0] - 1].DTlist[t[1]].center()[1], "gx",
                     markersize=12, linewidth=4)
            allHits.append(t)

        for n in noiseHits:
            if n in trueHits: continue
            plt.plot(MB1.layers[n[0] - 1].DTlist[n[1]].center()[0], MB1.layers[n[0] - 1].DTlist[n[1]].center()[1], "rx",
                     markersize=12, linewidth=4)
            allHits.append(n)

        # Now do the magic

        # First sort all hits according to the layer
        allHits.sort(key=lambda x: x[0])
        alltimes.sort(key=lambda k: [k[1], k[0]])

        # First try to separate them:
        print("START: ", allHits)
        plot2 = plt.figure(2)
        plt.axis([0, 200, -5, 30])
        for i in range(0, len(trueHits_SL1)):
            trueHits_SL1[i] = tuple(trueHits_SL1[i])
        for i in range(0, len(trueHits_SL2)):
            trueHits_SL2[i] = tuple(trueHits_SL2[i])
        inicio= time()

        layer_1 = buscar(allHits, 1)
        layer_1.sort(key=lambda k: [k[1], k[0]])

        layer_2 = buscar(allHits, 2)
        layer_2.sort(key=lambda k: [k[1], k[0]])

        layer_3 = buscar(allHits, 3)
        layer_3.sort(key=lambda k: [k[1], k[0]])

        layer_4 = buscar(allHits, 4)
        layer_4.sort(key=lambda k: [k[1], k[0]])

        layer_5 = buscar(allHits, 5)
        layer_5.sort(key=lambda k: [k[1], k[0]])

        layer_6 = buscar(allHits, 6)
        layer_6.sort(key=lambda k: [k[1], k[0]])

        layer_7 = buscar(allHits, 7)
        layer_7.sort(key=lambda k: [k[1], k[0]])

        layer_8 = buscar(allHits, 8)
        layer_8.sort(key=lambda k: [k[1], k[0]])


        muones_SL1 = buscar_patron(layer_1, layer_2, layer_3, layer_4)
        trazas_SL1 = []
        lateral1 = lateralidades(muones_SL1)
        data_SL1=compare_with_reality(trueHits_SL1, muones_SL1)
        elapsed_SL1=time()-inicio
        t_SL2=time()
        muones_SL2 = buscar_patron(layer_5, layer_6, layer_7, layer_8)
        lateral2 = lateralidades(muones_SL2)
        trazas_SL2 = []
        data_SL2=compare_with_reality(trueHits_SL2, muones_SL2)
        elapsed_SL2=time()-t_SL2
        muones_finales = correlate_superlayers(muones_SL1, muones_SL2)
        data_SL=compare_with_reality(trueHits2, muones_finales)

        elapsed_time=time()-inicio

        test_values.append([k,elapsed_time,len(muones_SL1)+len(muones_SL2),data_SL[0],data_SL[1],data_SL[2],nNoise])
        values_SL.append([k,elapsed_SL1,len(muones_SL1),data_SL1[0],data_SL1[1],data_SL1[2],elapsed_SL2,
                          len(muones_SL2),data_SL2[0],data_SL2[1],data_SL2[2],z])


# PRUEBAS CON POO //NO ESTA DENTRO DEL TEST******
for i in range(0, len(muones_SL1)):
    print("--------------------------------------------------------")
    print("-------------- SUPERLAYER 1 ---------------------")
    print("--------------------------------------------------------")
    print(f"POSIBLE MUON {i + 1}")
    print(muones_SL1[i])
    print("Lateralidades posibles")
    print(lateral1[i])
    print("POSICIONES")
    print(obtener_posicion_hit(alltimes, muones_SL1[i]))
    posiciones = obtener_posicion_hit(alltimes, muones_SL1[i])
    traza = trazaMuon(muones_SL1[i], (lateral1[i]), posiciones)
    trazas_SL1.append(traza)
plot2 = plt.figure(2)
plt.axis([0, 200, -5, 30])
MB1.plot()
for traza in trazas_SL1:
    traza.descartar_muones()

    traza.generar_posiciones_relativas()

    traza.sacar_pendientes()

    plt.plot(traza.x_pos, traza.y_pos, 'rx', markersize=12, linewidth=4)

print("")
print("*********** CAMBIO DE SL************ ")
print("")

for i in range(0, len(muones_SL2)):
    print("--------------------------------------------------------")
    print("-------------- SUPERLAYER 2 ---------------------")
    print("--------------------------------------------------------")
    print(f"POSIBLE MUON {i + 1}")
    print(muones_SL2[i])
    print("Lateralidades posibles")
    print(lateral2[i])
    posiciones = obtener_posicion_hit(alltimes, muones_SL2[i])
    traza = trazaMuon(muones_SL2[i], (lateral2[i]), posiciones)
    trazas_SL2.append(traza)

for traza in trazas_SL2:
    traza.descartar_muones()

    traza.generar_posiciones_relativas()

    traza.sacar_pendientes()

    plt.plot(traza.x_pos, traza.y_pos, 'rx', markersize=12, linewidth=4)


# ***********************************************************
# GRAFICO LOS PUNTOS
# ***********************************************************



print("==========================================================")
print("==========================================================")
print("==========================================================")
print("==========================================================")

# Plot it
if SL_mode:
    d = {'No.muones reales': [], 'Tiempo ejecucion SL1': [], 'Patrones totales SL1': [], 'No.muones reales DH SL1': [],
         'Muones no detectados SL1': [], '% acierto SL1': [], 'Tiempo ejecucion SL2': [], 'Patrones totales SL2': [],
         'No.muones reales DH SL2': [],
         'Muones no detectados SL2': [], '% acierto SL2': [], 'Noise': []}
    for x in values_SL:
        d['No.muones reales'].append(x[0])
        d['Tiempo ejecucion SL1'].append(x[1])
        d['Patrones totales SL1'].append(x[2])
        d['No.muones reales DH SL1'].append(x[3])
        d['Muones no detectados SL1'].append(x[4])
        d['% acierto SL1'].append(x[5])
        d['Tiempo ejecucion SL2'].append(x[6])
        d['Patrones totales SL2'].append(x[7])
        d['No.muones reales DH SL2'].append(x[8])
        d['Muones no detectados SL2'].append(x[9])
        d['% acierto SL2'].append(x[10])
        d['Noise'].append(x[11])

    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_normal_SL.csv',
              index=None)
if correlate_mode:
    d = {'No.muones reales': [], 'Tiempo ejecucion': [], 'Patrones totales': [], 'No.muones reales DH': [],
         'Muones no detectados': [], '% acierto': [], 'Noise': []}
    for x in test_values:
        d['No.muones reales'].append(x[0])
        d['Tiempo ejecucion'].append(x[1])
        d['Patrones totales'].append(x[2])
        d['No.muones reales DH'].append(x[3])
        d['Muones no detectados'].append(x[4])
        d['% acierto'].append(x[5])
        d['Noise'].append(x[6])

    jsonString = json.dumps(d)
    df = pd.read_json (jsonString)
    df.to_csv (r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_triangle_age.csv', index = None)

plt.show()
