import collections
import copy
import itertools
from random import shuffle
import json
from stationsObjects import *
import pandas as pd
from time import time


#FUNCIONES PARA EL DESARROLLO DEL ALGORITMO#

def count_elapsed_time(f):
    """
    Decorator.
    Execute the function and calculate the elapsed time.
    Print the result to the standard output.
    """

    def wrapper():
        # Start counting.
        start_time = time()
        # Take the original function's return value.
        ret = f()
        # Calculate the elapsed time.
        elapsed_time = time() - start_time
        print("Elapsed time: %0.10f seconds." % elapsed_time)
        return ret

    return wrapper

#BUSCAR EL VALOR MAS CERCANO EN UNA ARRAY (PARA BUSCAR CERCANOS EN EL ACUMULADOR)
def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


def search(lista, y):
    hits_1 = []

    for sublista in lista:
        if sublista[0] == y:
            hits_1.append(sublista)

    return hits_1

# TRANSFORMAR FORMATO CELDA A FORMATO XY
def cell2xy(hit):
    if hit[0] >= 5:
        ypos = (hit[0] - 1) * globalDTheight + globalDTheight / 2 + SLgap
    else:

        ypos = (hit[0] - 1) * globalDTheight + globalDTheight / 2

    if hit[0] % 2 == 0:
        xpos = (hit[1]) * globalDTwidth + globalDTwidth
    else:
        xpos = (hit[1]) * globalDTwidth + globalDTwidth / 2

    return ypos, xpos

#TRANSFORMAR FORMATO XY A FORMATO CELDA
def xy2cell(hit):
    if hit[0] >= 5:
        ypos = (hit[0] - globalDTheight / 2 - SLgap) / globalDTheight + 1
    else:
        ypos = (hit[0] - globalDTheight / 2) / globalDTheight + 1

    if round(hit[0], 3) in [1.95, 4.55, 25.75, 28.35]:
        xpos = (hit[1] - globalDTwidth) / globalDTwidth
    else:
        xpos = (hit[1] - globalDTwidth / 2) / globalDTwidth

    return [round(ypos), round(xpos)]

#DIVIDIR ENLACES DOBLES
def double2simple(muones):
    muones_divididos = []
    for muon in muones:
        d = collections.defaultdict(list)

        for x in muon:
            d[x[0]].append(x)

        l = list(itertools.product(*d.values()))

        for i in range(len(l)):
            l[i] = list(l[i])

        muones_divididos.extend(l)

    return muones_divididos

#SABER SI EL MUON EN CUESTION PRESENTA UNA COMBINACION POSIBLE DE CELDAS

def possible_comb(muon):

    displacement = [0, ]
    origen = muon[0][1]

    possible = True
    if len(muon) > 4:
        origen2 = muon[4][1]
    for i in range(1, len(muon)):
        if i < 4:
            difference = muon[i][1] - origen
        else:
            if i == 4:
                displacement.append(0)
                continue
            difference = muon[i][1] - origen2

        if i % 2 == 0:

            if difference > 0:
                displacement.append(2)
            elif difference < 0:
                displacement.append(-2)
            else:
                displacement.append(0)
        else:
            if difference > 0:
                displacement.append(3)
            elif difference == 0:
                displacement.append(1)
            elif difference < -1:
                displacement.append(-3)
            else:
                displacement.append(-1)

    for i in range(len(displacement) - 1):
        if abs(displacement[i] - displacement[i + 1]) > 1:
            possible = False
    return possible


def total_patrones(muones):
    lista=[]
    for muon in muones:
        if possible_comb(muon):
            if muon not in lista:
                lista.append(muon)
    
    return len(lista)
            
def pos_relativa(muon):
    # tomo como origen la primera celda del triangulo (0) y calculo la posicion relativa de las demas en unidades de sem
    # icelda
    desplazamiento = [0, ]
    origen = muon[0][1]


    if len(muon) > 4:
        origen2 = muon[4][1]

    for i in range(1, len(muon)):
        if i < 4:
            diferencia = muon[i][1] - origen
        else:
            if i == 4:
                desplazamiento.append(0)
                continue
            diferencia = muon[i][1] - origen2

        if i % 2 == 0:

            if diferencia > 0:
                desplazamiento.append(2)
            elif diferencia < 0:
                desplazamiento.append(-2)
            else:
                desplazamiento.append(0)
        else:
            if diferencia > 0:
                desplazamiento.append(3)
            elif diferencia == 0:
                desplazamiento.append(1)
            elif diferencia < -1:
                desplazamiento.append(-3)
            else:
                desplazamiento.append(-1)

    return desplazamiento

#CALCVULAR LAS LATERALIDADES DE UN PATRON DE 4 HITS
def lateralidades(muon):
    # supongo 0 izquierda y 1 derecha

    combinaciones_posibles = []
    combinaciones_reales = []
    for a in range(0, 2):
        for b in range(0, 2):
            for c in range(0, 2):
                for d in range(0, 2):
                    full = [a, b, c, d]
                    combinaciones_posibles.append(full)



    combinaciones_pos = []

    pos_rel = np.asarray(pos_relativa(muon))

    for combinacion in combinaciones_posibles:

        comb = np.asarray(combinacion)

        prueba = pos_rel + comb
        desfase = []
        desfase = np.asarray(desfase)
        diff = []
        diff = np.asarray(diff)
        for i in range(0, len(prueba) - 1):
            desfase = np.append(desfase, prueba[i + 1] - prueba[i])

        for i in range(0, len(desfase) - 1):
            diff = np.append(diff, (abs(desfase[i + 1] - desfase[i])))

        diff = np.append(diff, abs(desfase[2] - desfase[0]))

        resultado = (diff[0] > 1 or diff[1] > 1 or diff[2] > 1)

        if not resultado:
            combinaciones_pos.append(combinacion)



    for combinacion in combinaciones_pos:
        for i in range(0,len(combinacion)):

            if combinacion[i] == 0:
                combinacion[i] = -1
            elif combinacion[i] == 1:
                combinacion[i] = 1

    return combinaciones_pos

#FILTRO DE CALIDAD DE MUONES

def muon_quality(muones):
    # LISTA DE CALIDADES
    # LISTA[0] MAXIMA CALIDAD (8 HITS)
    # LISTA[1] CALIDAD ALTA
    # LISTA[2] CALIDAD MEDIA
    # LISTA[3] CALIDAD BAJA

    lista = [[], [], [], [], []]

    for muon in muones:
        if possible_comb(muon):
            if len(muon) == 8:
                if muon not in lista[0]:
                    lista[0].append(muon)
                    print(f"Adding MAX QUALITY MUON  {muon}")
            elif len(muon) == 7:
                if muon not in lista[1]:
                    lista[1].append(muon)
                    print(f"Adding HIGH QUALITY MUON  {muon}")
            elif len(muon) == 6:
                if muon not in lista[2]:
                    lista[2].append(muon)
                    print(f"Adding MEDIUM QUALITY MUON  {muon}")
            elif len(muon) == 5:
                if muon not in lista[3]:
                    lista[3].append(muon)
                    print(f"Adding LOW QUALITY MUON  {muon}")
            elif len(muon) == 4:
                if muon not in lista[4]:
                    lista[4].append(muon)
                    print(f"Adding LOWLOW QUALITY MUON  {muon}")

    return lista

#EXTRAER MUONES DE CALIDAD X
def get_muon_quality(list, quality):
    if 0 <= quality < 5:
        print(f"Returning quality {quality} muons")
        return list[quality]
    else:
        print(""" quality must be fron 0 to 4 \n
                quality 0 MAXIMA CALIDAD (8 HITS)\n
                quality 1 CALIDAD ALTA\n   
                quality 2 CALIDAD MEDIA\n
    #           quality 3 CALIDAD BAJA\n """)


def count_quality_muons(muons_q):
    for i in range(4):
        n_muons = len(muons_q[i])
        if i == 0:
            quality = "MAX"
        elif i == 1:
            quality = "HIGH"
        elif i == 2:
            quality = "MEDIUM"
        elif i == 3:
            quality = "LOW"
        elif i == 4:
            quality=="LOWLOW"

        print("\n")

        print(f"There are {n_muons} muons with QUALITY: {quality} \n")


#FUNCION PRINCIPAL#
#DESARROLLO DEL ALGORITMO #3 BASADO EN LA TRANSFORMADA DE HOUGH#

def patrones_hough(p_x, p_y,intervalo_theta,intervalo_rho):
    # BUFFERS

    BUFFER_ENTRADA = []
    accumulators = []
    patrons = []

    # VALORES DE ANGULOS (50º MAX )
    thetas = np.deg2rad(np.arange(-50, 50, intervalo_theta))
    # TAMAÑO PLANO XY
    width, height = 200, 30
    # DIAGONAL PARA EL ACUMULADOR
    diag_len = float(np.ceil(np.sqrt(width * width + height * height)))  # max_dist
    # LINSPACE DE RHOS
    rhos = np.arange(-diag_len, diag_len, intervalo_rho)

    # Guardo valores
    cos_t = np.cos(thetas)
    sin_t = np.sin(thetas)
    num_thetas = len(thetas)
    num_rhos = len(rhos)

    total_accumulator = np.zeros((num_rhos, num_thetas))

    # PUNTOS
    y_idxs, x_idxs = p_y, p_x

    # [0.65,4.55,24.45,28.35], [100,106.3,20,26.3]

    # VOTOS EN EL ACUMULADOR
    for i in range(len(x_idxs)):
        x = x_idxs[i]
        y = y_idxs[i]
        BUFFER_ENTRADA.append(xy2cell([y, x]))

        #  Acumulador (ARRAY THETA VS RHO)
        accumulator = np.zeros((num_rhos, num_thetas))

        for t_idx in range(num_thetas):
            # VOY CALCULADO VALORES DE RHO. AÑADO DIAGONAL PARA INDICE POSITIVO
            rho = (x * cos_t[t_idx] + y * sin_t[t_idx])
            r_idx = find_nearest(rhos, rho)
            accumulator[int(r_idx), int(t_idx)] += 1
            total_accumulator[int(r_idx), int(t_idx)] += 1

        accumulators.append(accumulator)
    # tengo el acumulador completo
    # donde hay valores maximos?
    accumulators = np.array(accumulators)
    idxs = []
    valor_max = np.amax(total_accumulator)
    for i in range(int(valor_max), 4, -1):

        idx_valores_max = np.where(total_accumulator == i)
        print(f'Para los puntos con  acumulaciones: {i}:')
        for j in range(len(idx_valores_max[0])):
            patron = []
            element = [idx_valores_max[0][j], idx_valores_max[1][j]]

            for k in range(len(accumulators)):

                if accumulators[k][element[0]][element[1]] == 1:
                    if BUFFER_ENTRADA[k] not in patron:
                        patron.append(BUFFER_ENTRADA[k])

            print("-----")
            if len(patron) > 3:
                patron.sort(key=lambda x: (x[0], x[1]))
                patrons.append(patron)
                print(patron)
    # fig, ax = plt.subplots(1)
    #
    # plt.imshow(total_accumulator, cmap='jet',
    #            extent=[np.rad2deg(thetas[-1]), np.rad2deg(thetas[0]), rhos[-1], rhos[0]])
    # plt.colorbar()
    # plt.show(block=False)
    return patrons,total_accumulator

#FUNCION QUE COMPARA CON MUONES REALES EL RESULTADO.
def compare_with_reality(trueMuons, algorithmMuons):
    total = len(trueMuons)
    matched = 0
    already_matched = []

    for muon in algorithmMuons:
        for real_muon in trueMuons:
            if muon == real_muon and not muon in already_matched:
                matched += 1
                already_matched.append(muon)

    wrong = total - matched
    if not total == 0:
        percentage = matched / total * 100
    else:
        percentage = 0

    print(f"""RESULTS: \n
            *************************
            NUMBER OF TRUE MUONS : {total} \n
            NUMBER OF MUONS FROM ALGORITHM : {len(algorithmMuons)} \n
            NUMBER OF MUONS NOT PREDICTED: {wrong} \n
            PERCENTAGE OF CORRECTNESS: {percentage} \n
            NOISE HITS : {nNoise}
            *************************
""")
    return [total,len(algorithmMuons),wrong,percentage]

#APLICAR AGING A TRUEMUONS
def edit_aging_truehits(truemuons, aging):
    for hit in aging:
        for truemuon in truemuons:
            for cell in truemuon:
                if cell == hit:
                    truemuon.remove(cell)
    for muon in truemuons:
        if len(muon) < 3:
            truemuons.remove(muon)

#DIVIDIR LOS TRUEHITS POR SEMICELDAS

def truehits_SL(truehits, semicells):
    true_SL1 = []
    true_SL2 = []
    lat_SL1 = []
    lat_SL2 = []

    for hit in truehits:
        if len(hit) == 4:
            if possible_comb(hit):
                idx = truehits.index(hit)
                true_SL1.append(hit)
                lat_SL1.append(semicells[idx])
        else:
            for i in range(0, len(hit)):
                a = hit[i][0]
                if a > 4:
                    if hit[:i] not in true_SL1 and possible_comb(hit[:i]):
                        true_SL1.append(hit[:i])
                        idx = truehits.index(hit)
                        lat_SL1.append(semicells[idx][:i])
                    if hit[i:] not in true_SL2 and possible_comb(hit[i:]):
                        true_SL2.append(hit[i:])
                        idx = truehits.index(hit)
                        lat_SL2.append(semicells[idx][i:])
                    break

    return true_SL1, true_SL2, lat_SL1, lat_SL2

def lat_to_truehits(truehits, semicells):
    orden_data = []
    true_lats = []
    for i in range(0, len(truehits)):
        for hit in truehits[i]:
            orden_data.append([hit, semicells[i][truehits[i].index(hit)]])
    real_true = double2simple(truehits)

    for muon in real_true:
        lat_list = []
        for hit in muon:
            for data in orden_data:
                if hit == data[0]:
                    lat_list.append(data[1])
        true_lats.append(lat_list)

    return real_true, true_lats

def compare_aging(patronesSL1, trueSL1, hitsAging):
    total_SL1 = len(trueSL1)
    
    edit_aging_truehits(trueSL1, hitsAging)
    
    matched_sl1 = 0
    already_matched_sl1 = []

    for muon in patronesSL1:
        for real_muon in trueSL1:
            if muon == real_muon and not muon in already_matched_sl1:
                matched_sl1 += 1
                already_matched_sl1.append(muon)


    wrong_sl1 = total_SL1 - matched_sl1

    if not total_SL1 == 0:
        percentage_sl1 = matched_sl1 / total_SL1 * 100
    else:
        percentage_sl1 = 0



    return total_SL1, wrong_sl1, percentage_sl1

def asign_lateralities(patrones):
    patron_lat=[]
    numero=0

    for patron in patrones:
        length = len(patron)
        middle_index = length // 2

        first_half = patron[:middle_index]

        second_half = patron[middle_index:]

        lateralidad1 = lateralidades(first_half)

        lateralidad2 = lateralidades(second_half)

        numero=numero+len(lateralidad1)+len(lateralidad2)
        patron_lat.append([patron, lateralidad1, lateralidad2])


    return patron_lat, numero

#COMPARAR LATERALIDADES DE LOS PATRONES CON LAS REALES

def compare_lateralities(patron_lat, truemuons, truelateralities, numero):
    total_lat=numero
    total_lat_true=len(truelateralities)
    matched = 0
    

    for patron in patron_lat:
        for muon in truemuons:
            if patron[0]==muon:
                idx=truemuons.index(muon)
                for lat in patron[1]:
                    for lat2 in patron[2]:
                        lat_total=[]
                        lat_total.extend(lat)
                        lat_total.extend(lat2)
                        truelat=truelateralities[idx]
                        if lat_total==truelat:
                            matched +=1

    wrong_lat = total_lat_true - matched
    if not total_lat_true == 0:
        percentage = matched / total_lat_true * 100
    else:
        percentage = 0

    return total_lat,total_lat_true,wrong_lat,percentage

#TESTING
#LISTAS PARA ALMACENAR LOS DATOS DEL TEST#
hitstest=[]
testt=[]
valores=[]
lateralities_test = []
aging_test_values = []

#LAT MODE -> OBTENER LATERALIDADES
#AGE_MODE -> APLICAR ENVEJECIMIENTO
#NORMAL_MODE -> TEST NORMAL CON RUIDO
lat_mode=False
age_mode=False
normal_mode=True

#j = Numero de muones reales
for j in range(1,11):
    for w in range(0,201,25): #intervalo de ruido, desde 0 a 200 de 25 en 25
        for z in range(2,20,2): #intervalo rho/theta
            for u in range(0,1): #aging
                # Global parameters
                np.random.seed(4313937)

                # np.random.seed(1)
                nTrueMuons = j

                nNoise = w


                reMatchHits = True
                doLaterality = True
                if age_mode:
                    if u == 0:
                        agingPercentage = 0.94
                    elif u == 1:
                        agingPercentage = 0.85
                    elif u == 2:
                        agingPercentage = 0.62
                else:
                    agingPercentage = 1

                minHits = 2

                # Loads the patterns
                # fig, ax = plt.subplots(1)
                #
                # plt.axis([0, 200, -5, 30])

                # Now test a set of hits

                # Generate true muons and its resulting set of patterns
                allHits = []
                trueHits = []
                trueHits2 = []
                true3 =[]
                trueMuons = []
                hitsSL1 = []
                hitsSL2 = []
                ageHits = []
                trueLats = []
                for n in range(nTrueMuons):
                    mm = Muon(np.random.rand() * 200, 0., 1. / (np.random.rand()))
                    MB1.checkIn(mm)
                    trueHits += mm.getRecoPattern()
                    trueLats.append(mm.semicells)
                    trueHits2.append(mm.getRecoPattern())
                    mm.color = "g--"
                    mm.plot()
                    trueMuons.append(mm)

                # And now generate a couple of points with random noise
                noiseHits = []
                for n in range(nNoise):
                    noiseHits.append([int(np.ceil(np.random.rand() * 8)), int(np.floor(np.random.rand() * 47))])

                for t in trueHits:
                    if np.random.rand() > agingPercentage:
                        ageHits.append(t)
                        continue
                    plt.plot(MB1.layers[t[0] - 1].DTlist[t[1]].center()[0], MB1.layers[t[0] - 1].DTlist[t[1]].center()[1], "gx",
                             markersize=12, linewidth=4)
                    allHits.append(t)

                for n in noiseHits:
                    if n in trueHits: continue
                    plt.plot(MB1.layers[n[0] - 1].DTlist[n[1]].center()[0], MB1.layers[n[0] - 1].DTlist[n[1]].center()[1], "rx",
                             markersize=12, linewidth=4)
                    allHits.append(n)

                # Now do the magic
                shuffle(allHits)

                # First sort all hits according to the layer

                # allHits.sort(key=lambda x: (x[0], x[1]))
                for hit in allHits:
                    if hit[0] < 5:
                        hitsSL1.append(hit)
                    else:
                        hitsSL2.append(hit)

                # First try to separate them:
                print("START: ", allHits)



                true3,true_lat=lat_to_truehits(trueHits2,trueLats)

                trueHits2 = double2simple(trueHits2)
                position = 0
                while position < len(trueHits2):

                    if not possible_comb(trueHits2[position]):
                        trueHits2.pop(position)
                    else:
                        position = position + 1




                allHitsxy = copy.deepcopy(allHits)

                for i in range(len(allHits)):
                    allHitsxy[i][0], allHitsxy[i][1] = cell2xy(allHits[i])

                lista = np.array(allHitsxy)
                puntos_x = lista[:, 1]
                puntos_y = lista[:, 0]


                start_time=time()

                #aplicamos algoritmo
                hough=patrones_hough(puntos_x, puntos_y, z, z)


                patrons = double2simple(hough[0])
                elapsed_time = time() - start_time
                patrones_totales = total_patrones(patrons)
                tru_muons = muon_quality(patrons)
                count_quality_muons(tru_muons)



                if age_mode:
                    data_aging = compare_aging(patrons, trueHits2, ageHits)
                    aging_test_values.append(
                        [j, agingPercentage, data_aging[0], data_aging[1], data_aging[2]])
                if lat_mode:

                    patrones_lat=asign_lateralities(tru_muons[0])
                    data_lat=compare_lateralities(patrones_lat[0],true3,true_lat,patrones_lat[1])
                    lateralities_test.append([j, data_lat[0], data_lat[1], data_lat[2], data_lat[3]])
                data = compare_with_reality(trueHits2, tru_muons[0])

                print("Elapsed time: %.10f seconds." % elapsed_time)
                valores.append([j,elapsed_time,patrones_totales,data[0],data[1],data[2],data[3],w,z])


valoress=np.array(valores)

#CREO CSV DE DATOS, COMPRIMO Y EXPORTO.

if normal_mode:
    d = {'No.muones reales':[],'Tiempo ejecucion':[],'Patrones totales':[],'Patrones algoritmo':[],'No.muones reales DH':[],'Muones no detectados':[],'% acierto':[],'Noise':[],'Intervalo theta/rho':[]}
    for x in valores:
        d['No.muones reales'].append(x[0])
        d['Tiempo ejecucion'].append(x[1])
        d['Patrones totales'].append(x[2])
        d['Patrones algoritmo'].append(x[4])
        d['No.muones reales DH'].append(x[3])
        d['Muones no detectados'].append(x[5])
        d['% acierto'].append(x[6])
        d['Noise'].append(x[7])
        d['Intervalo theta/rho'].append(x[8])

    jsonString = json.dumps(d)
    df = pd.read_json (jsonString)
    df.to_csv (r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_hough_normal.csv', index = None)
plt.show()
if age_mode:
    d = {'No.muones reales': [], 'Aging': [], 'Patrones totales': [], 'Muones no detectados': [],
         'Efectividad': []}
    for x in aging_test_values:
        d['No.muones reales'].append(x[0])
        d['Aging'].append(x[1])
        d['Patrones totales'].append(x[2])
        d['Muones no detectados'].append(x[3])
        d['Efectividad'].append(x[4])


    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_hough_aging.csv',
              index=None)

if lat_mode:
    d = {'No.Muones reales':[],'No.lateralidades totales': [], 'No.lateralidades': [], 'Lat. no detectadas': [], 'Efectividad LAT': []}
    for x in lateralities_test:
        d['No.Muones reales'].append(x[0])
        d['No.lateralidades totales'].append(x[1])
        d['No.lateralidades'].append(x[2])
        d['Lat. no detectadas'].append(x[3])
        d['Efectividad LAT'].append(x[4])


    jsonString = json.dumps(d)
    df = pd.read_json(jsonString)
    df.to_csv(r'C:\Users\Pelayo Leguina\Documents\INFO TFG-CMS ALGORITHMS\DOCUMENTO\TESTS\test_hough_lateral.csv', index=None)



# VALORES DE ANGULOS (50º MAX )
thetas = np.deg2rad(np.arange(-50, 50, 5))
# TAMAÑO PLANO XY
width, height = 200, 30
# DIAGONAL PARA EL ACUMULADOR
diag_len = float(np.ceil(np.sqrt(width * width + height * height)))  # max_dist
# LINSPACE DE RHOS
rhos = np.arange(-diag_len, diag_len, 5)
fig, ax = plt.subplots(1)

plt.imshow(hough[1], cmap='jet',
            extent=[np.rad2deg(thetas[-1]), np.rad2deg(thetas[0]), rhos[-1], rhos[0]])
plt.colorbar()
plt.show(block=False)





print("done")
